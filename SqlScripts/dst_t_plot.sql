SELECT temperature,time_stamp FROM DST
WHERE time_stamp BETWEEN DATE_SUB(NOW(), INTERVAL 10 HOUR) AND NOW()
ORDER BY time_stamp;
