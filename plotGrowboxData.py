import matplotlib.pyplot as plt
import numpy as np
import mysql.connector
import matplotlib.dates as mdates
import sys

def readDatabase(cursor,sql_file):
    with open(sql_file, 'r') as file:
        result_iterator = cursor.execute(file.read(), multi=True)
        for res in result_iterator:
            print("Running query")
            if res.with_rows:
                    return res.fetchall()
            else:
                    return []
    pass
def justMakePlot(xlabel,ylabel,title,colour,xaxis_data,yaxis_data,output):
    print("Plotting")
    fig, ax = plt.subplots()  # Create a canvas containing a single axes.
    ax.plot(xaxis_data, yaxis_data, color=colour)  # Plot some data on the axes.
    ax.set_xlabel(xlabel)  # Add an x-label to the axes.
    ax.set_ylabel(ylabel)  # Add a y-label to the axes.
    ax.set_title(title)  # Add a title to the axes.
    #label tacks on x setting
    max_xticks = 10
    xloc = plt.MaxNLocator(max_xticks)
    ax.xaxis.set_major_locator(xloc)

    xformatter = mdates.DateFormatter('%H:%M')
    plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)
    print("Exporting")
    plt.savefig(output, format="svg")

    pass

if len(sys.argv)==1:
    print("Expected argument list is as follows:")
    print("xaxis label | yaxis label | plot title | line color | mysql script file | output file")
    print("This script expects sql query to return a touple (variable, datetime)")
    exit(0)
    pass
if len(sys.argv)!=7:
    print("Argument list error")
    exit(0)
    pass

db = mysql.connector.connect(
  database="growbox_db",
  host="localhost",
  user="growbox_user",
  password="dfc0af598b4"
)
cursor = db.cursor()

data=readDatabase(cursor,sys.argv[5])
print("Preparing data")
xaxis_data=[]
yaxis_data=[]
#plot for temp
for row in data:
    xaxis_data.append(row[1])
    yaxis_data.append(row[0])
    pass
#xaxis_data = date2num(xaxis_data)
justMakePlot(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],xaxis_data,yaxis_data,sys.argv[6])

#script expects sql query to return a touple (variable, datetime)
#xaxis label | yaxis label | plot title | line color | mysql script file | output file
