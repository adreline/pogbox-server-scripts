#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games

#read dht11
access_point="/home/pi/growbox/dht11_interface.py"
dht11=$(python3 $access_point)

i=0
while [[ "404" == "$dht11" ]]; do
  dht11=$(python3 $access_point)
  if [[ $i>50 ]]; then
    break
  fi
  ((i++))
done
#insert data
if [[ "404" == "$dht11" && "" != "$dht11" ]]; then
  #read has failed after 50 attempts, do not enter a record
  echo "DHT11 read failed after 50 attempts"
else
  #read successful
  a=$(echo $dht11|awk '{ print $1 }')
  b=$(echo $dht11|awk '{ print $2 }')
  echo "inserting "$a" and "$b
  echo "INSERT INTO DHT ( id,temperature,humidity,time_stamp) VALUES ( null,$a,$b,NOW());"| mysql -ugrowbox_user -pdfc0af598b4 growbox_db
fi


#read dht11 2
access_point="/home/pi/growbox/dht11_2_interface.py"
dht11_2=$(python3 $access_point)

i=0
while [[ "404" == "$dht11_2" ]]; do
  dht11_2=$(python3 $access_point)
  if [[ $i>50 ]]; then
    break
  fi
  ((i++))
done
#insert data
if [[ "404" == "$dht11_2" && "" != "$dht11_2" ]]; then
  #read has failed after 50 attempts, do not enter a record
  echo "DHT11 read failed after 50 attempts"
else
  #read successful
  a=$(echo $dht11_2|awk '{ print $1 }')
  b=$(echo $dht11_2|awk '{ print $2 }')
  echo "inserting "$a" and "$b
  echo "INSERT INTO DHT2 ( id,temperature,humidity,time_stamp) VALUES ( null,$a,$b,NOW());"| mysql -ugrowbox_user -pdfc0af598b4 growbox_db
fi


#read dst
access_point="/sys/bus/w1/devices/28-0516927123ff/w1_slave"
if test -f "$access_point"; then
  dst=$(cat $access_point | tail -c 6)
  dst=$(( $dst/1000 ))"."$(( $dst%1000 ))
  echo "inserting "$dst
  echo "INSERT INTO DST ( id,temperature,time_stamp) VALUES ( null,$dst,NOW());"| mysql -ugrowbox_user -pdfc0af598b4 growbox_db
fi
