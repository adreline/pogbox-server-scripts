cd /home/pi/growbox
python3 plotGrowboxData.py Data "%" "Wilogoć" "#A5A58D" SqlScripts/dht1_h_plot.sql PlotFiles/dht1_h_plot.svg
python3 plotGrowboxData.py Data "C" "Temperatura" "#DDBEA9" SqlScripts/dht1_t_plot.sql PlotFiles/dht1_t_plot.svg
python3 plotGrowboxData.py Data "%" "Wilogoć" "#A5A58D" SqlScripts/dht2_h_plot.sql PlotFiles/dht2_h_plot.svg
python3 plotGrowboxData.py Data "C" "Temperatura" "#DDBEA9" SqlScripts/dht2_t_plot.sql PlotFiles/dht2_t_plot.svg
python3 plotGrowboxData.py Data "C" "Temperatura" "#DDBEA9" SqlScripts/dst_t_plot.sql PlotFiles/dst_t_plot.svg
