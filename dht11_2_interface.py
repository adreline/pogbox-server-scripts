import RPi.GPIO as GPIO
import dht11

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# read data using pin 14
instance = dht11.DHT11(pin = 26)
result = instance.read()

if result.is_valid():
    print(str(result.temperature)+" "+str(result.humidity))
else:
    print("404")
